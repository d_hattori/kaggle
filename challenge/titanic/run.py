import pandas as pd
from sklearn.tree import DecisionTreeRegressor
import numpy as np
from sklearn.linear_model import Ridge
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV, KFold
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn import svm
from sklearn.tree import DecisionTreeClassifier
from sklearn.preprocessing import LabelEncoder
from xgboost import XGBClassifier
import itertools
from sklearn.metrics import log_loss, accuracy_score
from preprocess import preprocess, encoding

pd.set_option('display.max_columns',100)
train = pd.read_csv('input_csv/train.csv')
test_x_origin = pd.read_csv('input_csv/test.csv')
train_x, train_y, test_x= preprocess(train, test_x_origin)
train_x, test_x= encoding(train_x, test_x)

print(test_x.head)

param_space = {
    'max_depth': [3,5,7],
    'min_child_weight': [1.0, 2.0, 4.0]
}
param_combinations = itertools.product(param_space['max_depth'], param_space['min_child_weight'])
params = []
scores = []
scores_logloss = []
scores_accuracy = []
te_ys = []
for max_depth, min_child_weight in param_combinations:
    score_folds = []
    te_y_folds =  []
    kf = KFold(n_splits=4, shuffle=True, random_state=71)
    
    for tr_idx, va_idx in kf.split(train_x):
        tr_x, va_x = train_x.iloc[tr_idx], train_x.iloc[va_idx]
        tr_y, va_y = train_y.iloc[tr_idx], train_y.iloc[va_idx]
        
        model = XGBClassifier(
            n_estimators=20,
            max_depth=max_depth,
            min_child_weight=min_child_weight
        )
        # model = DecisionTreeClassifier(random_state=1)
        
        model.fit(tr_x, tr_y)
        va_pred = model.predict_proba(va_x)[:, 1]

        te_x = test_x.values
        te_pred = model.predict_proba(te_x)[:, 1]
        accuracy = accuracy_score(va_y, va_pred>0.5)
        score_folds.append(accuracy)
        te_y_folds.append(te_pred)
        print(te_pred)
    te_y_folds = np.array(te_y_folds)
    score_mean = np.mean(score_folds)
    params.append((max_depth, min_child_weight))
    scores.append(score_mean)
    te_y_mean = np.mean(te_y_folds,axis=0)
    te_ys.append(te_y_mean)
    
te_ys = np.array(te_ys)
best_idx = np.argsort(scores)[0]
best_param = params[best_idx]
bast_score = scores[best_idx]
te_y = te_ys[best_idx]
print(te_ys)
print(best_param)
print(f"accuracy = {bast_score:.4f}")

submit = pd.DataFrame()
submit["PassengerId"] = test_x_origin["PassengerId"]
submit["Survived"] = np.where(te_y>=0.5, 1, 0)
submit.to_csv("rusult.csv", index = False)
# print(te_y)

