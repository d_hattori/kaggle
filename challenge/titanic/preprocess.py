import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OrdinalEncoder
import seaborn as sns
def preprocess(train_data,test_data):
    # merge train & test data
    all_data = pd.concat([train_data.drop(['Survived'],axis=1), test_data], axis=0)
    print(all_data.head)
    # split name
    all_name_split_conmma = all_data["Name"].str.split(",", expand=True)
    all_name_split_piriod = all_name_split_conmma[1].str.split(".", expand=True)
    preprocessed_all_data = all_data.drop(["PassengerId"], axis=1)
    preprocessed_all_data["Name_Called"] = all_name_split_piriod[0].str.replace(' ','')
    preprocessed_all_data["Name_First"] = all_name_split_piriod[1]
    preprocessed_all_data = preprocessed_all_data.drop(["Name"], axis=1)

    # split cabin
    # cabin_split = preprocessed_all_data["Cabin"].str.split(" ", expand=True)
    # for c_cabin in cabin_split.columns:
    #     preprocessed_all_data[f"Cabin_Stair_{c_cabin}"] = cabin_split[c_cabin].str[0]
    #     preprocessed_all_data[f"Cabin_Room_{c_cabin}"] = pd.to_numeric(cabin_split[c_cabin].str[1:], errors="coerce")
    preprocessed_all_data = preprocessed_all_data.drop(["Cabin"], axis=1)
    preprocessed_all_data = preprocessed_all_data.drop(["Ticket"], axis=1)
    preprocessed_all_data = preprocessed_all_data.drop(["Name_First"], axis=1)

    # Name_Called grouping
    preprocessed_all_data['Name_Called'].replace(['Capt', 'Col', 'Major', 'Dr', 'Rev'], 'Officer', inplace=True)
    preprocessed_all_data['Name_Called'].replace(['Don', 'Sir',  'theCountess', 'Lady', 'Dona', 'Jonkheer'], 'Royalty', inplace=True)
    preprocessed_all_data['Name_Called'].replace(['Mme', 'Ms'], 'Mrs', inplace=True)
    preprocessed_all_data['Name_Called'].replace(['Mlle'], 'Miss', inplace=True)
    preprocessed_train_data = preprocessed_all_data[:890]
    preprocessed_test_data = preprocessed_all_data[891:]
    return preprocessed_train_data, train_data['Survived'], preprocessed_test_data
    
def encoding(preprocessed_train_data,preprocessed_test_data):
    preprocessed_all_data = pd.concat([preprocessed_train_data, preprocessed_test_data], axis=0)
    # label encode on sex and embarked name_called
    for c in ['Sex', 'Embarked', 'Name_Called', 'Pclass']:
        le = LabelEncoder()
        le.fit(preprocessed_all_data[c].fillna('NA'))
        preprocessed_train_data[c] = le.transform(preprocessed_train_data[c].fillna('NA'))
        preprocessed_test_data[c] = le.transform(preprocessed_test_data[c].fillna('NA'))
    
    # # label encode on cabin each stair
    # le.fit(preprocessed_all_data['Cabin_Stair_0'].fillna('NA'))
    # for c in ['Cabin_Stair_0', 'Cabin_Stair_1', 'Cabin_Stair_2', 'Cabin_Stair_3']:
    #     preprocessed_train_data[c] = le.transform(preprocessed_train_data[c].fillna('NA'))
    #     preprocessed_test_data[c] = le.transform(preprocessed_test_data[c].fillna('NA'))
    
    # # #label encode on Pclass and sex
    # oe = OrdinalEncoder()
    # preprocessed_all_data.loc[:,['PclassEncoded', 'SexEncoded']] = oe.fit_transform(preprocessed_all_data[['Pclass', 'Sex']])
    # preprocessed_train_data.loc[:,['PclassEncoded', 'SexEncoded']] = oe.transform(preprocessed_train_data[['Pclass', 'Sex']])
    # preprocessed_test_data.loc[:,['PclassEncoded', 'SexEncoded']] = oe.transform(preprocessed_test_data[['Pclass', 'Sex']])

    return preprocessed_train_data, preprocessed_test_data
   
pd.set_option('display.max_columns',100)
train = pd.read_csv('input_csv/train.csv')
test_x_origin = pd.read_csv('input_csv/test.csv')
train_x, train_y, test_x= preprocess(train, test_x_origin)
train_x, test_x= encoding(train_x, test_x)

print(train_x["Sex"])
numeric = train_x
numeric["target"] = train_y
print(train_x.info())
# print(test_x.info())
# # numeric = train_x[["Pclass", "Age", "SibSp", "SibSp"]]
# numeric = numeric.drop(["Sex", "Embarked", "Name_Called", "Cabin_Stair_0", "Cabin_Stair_1", "Cabin_Stair_2", "Cabin_Stair_3"], axis=1)
# print(numeric.info())
fig = numeric.iloc[:, 1:].corr().style.background_gradient(axis=None)
fig.to_excel("background_gradient.xlsx")

# all = train_x.copy()
# all["Survived"] = train_y
# name_called_survived = sns.barplot(x='Name_Called', y='Survived', data=all, palette='Set2')
# figure = name_called_survived.get_figure()
# figure.savefig("name_called_survived.png")