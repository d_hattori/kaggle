import pandas as pd
from sklearn.tree import DecisionTreeRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn import linear_model

melbourne_file_path = 'melb_data.csv'
melbourne_data = pd.read_csv(melbourne_file_path) 
melbourne_data = melbourne_data.dropna(axis=0) # 欠損値の削除

# targetの選択
y = melbourne_data.Price
# featureの選択
melbourne_features = ['Rooms', 'Bathroom', 'Landsize', 'Lattitude', 'Longtitude']
X = melbourne_data[melbourne_features]

# Define model. Specify a number for random_state to ensure same results each run
melbourne_model = DecisionTreeRegressor(random_state=1)
# Fit model
melbourne_model.fit(X, y)

print("Making predictions for the following 5 houses:")
print(X.head())

print("The predictions are")
print("decision tree")
print(y.head().values)
print(melbourne_model.predict(X.head()))
clf = linear_model.LinearRegression()
clf.fit(X,y)
print("decision tree")
print(y.head().values)
print(clf.predict(X.head()))
